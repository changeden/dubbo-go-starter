# gitee.com/changeden/dubbo-go-starter

go语言网络服务启动套件，打造go版本的spring-boot。

## 组件

| 名称                           | 包                                                                                                            | 描述         |  最新版本  |
|:-----------------------------|:-------------------------------------------------------------------------------------------------------------|:-----------|:------:|
| dubbo-go-middleware-database | [gitee.com/changeden/dubbo-go-middleware-database](https://gitee.com/changeden/dubbo-go-middleware-database) | 数据库中间件     | v0.1.5 |
| dubbo-go-middleware-redis    | [gitee.com/changeden/dubbo-go-middleware-redis](https://gitee.com/changeden/dubbo-go-middleware-redis)       | 内存缓存中间件    | v0.1.5 |
| dubbo-go-middleware-web      | [gitee.com/changeden/dubbo-go-middleware-web](https://gitee.com/changeden/dubbo-go-middleware-web)           | Web服务中间件   | v0.1.5 |
| dubbo-go-middleware-dubbo    | [gitee.com/changeden/dubbo-go-middleware-dubbo](https://gitee.com/changeden/dubbo-go-middleware-dubbo)       | dubbo服务中间件 | v0.1.5 |


## 样本
[https://gitee.com/changeden/dubbo-go-project-sample](https://gitee.com/changeden/dubbo-go-project-sample)
