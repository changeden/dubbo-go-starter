module gitee.com/changeden/dubbo-go-starter

go 1.16

require (
	dubbo.apache.org/dubbo-go/v3 v3.0.1
	github.com/go-redis/redis/v8 v8.11.4
	gopkg.in/yaml.v2 v2.4.0
)
