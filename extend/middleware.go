package extend

import (
	"gitee.com/changeden/dubbo-go-starter/model"
)

type DubboGoMiddlewareI interface {
	Setup(model.ApplicationConfig, []DubboGoMiddlewareSetupHook) error

	IsAsync() bool

	Shutdown()
}

type DubboGoMiddlewareSetupHook interface {
	Hook()
}
