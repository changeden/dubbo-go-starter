package common

import (
	"gitee.com/changeden/dubbo-go-starter/extend"
	"gitee.com/changeden/dubbo-go-starter/model"
)

var (
	Config             model.ApplicationConfig
	DubboGoMiddlewares []extend.DubboGoMiddlewareI

	ConfigPath      string
	DubboConfigPath string
)
