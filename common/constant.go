package common

// System Config
const (
	ApplicationConfigFilePathKey     = "CONF_APPLICATION_FILE_PATH"
	DefaultApplicationConfigFilePath = "resources/application.yml"
	DubboConfigFilePathKey           = "DUBBO_GO_CONFIG_PATH"
)
